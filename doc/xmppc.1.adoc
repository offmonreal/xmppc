XMPPC(1)
========

NAME
----
xmppc - XMPP Command line Tool 


SYNOPSIS
--------
'xmppc' ['OPTIONS'] -m 'MODE' 'COMMAND' [<args>...]

'xmppc' [-a 'ACCOUNT'] [-j 'JID'] [-p 'PASSWORD']  -m 'MODE' 'COMMAND' [<args>...]


DESCRIPTION
-----------
xmppc is a XMPP command line tool. You need an XMPP Account to use this tool.
The account information can be defined via command line argument -j 'JID'. If no
password is provided via -p 'PASSWORD' xmppc will ask the user to enter the
password. Another option is to define 1 or more accounts within a configuration
file. The option -a 'ACCOUNT' can be used to select the account.

The argument -m 'MODE' defines the xmppc 'MODE' which should be used (roster,
message, pgp, omemo, openpgp, monitor, bookmark, mam, discovery). The 'COMMAND'
and the args required by the 'COMMAND' depends on the chosen 'MODE'.


OPTIONS
-------
*-a, --attribute* 'ACCOUNT'::
  'ACCOUNT' is a name of a xmpp account defined within the configuration file.

*-j, --jid* 'JID'::
  'JID' of the XMPP Account.

*-p, --pwd* 'PWD'::
  Password of the XMPP Account.

*-v* 'VERBOSE'::
  Verbose flags. -v[v[v[v]]]::
  -v is WARN -vv is INFO --vvv is DEBUG -vvvv is TRACE

*--help*::
    Print program version number and help


MODES AND COMMANDS
------------------
xmppc modes.

*-m roster*:: 
The Roster mode can be used to provided information of the xmpp account's
roster. The roster is the XMPP list of contacts. 

* *list* - List all contacts
* *export* - Exports all contacts

*-m message*:: 
The message mode can be used to send unencrypted messages to another xmpp
account.

* *chat <jid> <message>* - Sending unencrypted message to jid
* *groupchat <jid> <message>* - Sending unencrypted message to group chat


*-m pgp*:: 
PGP Mode (XEP-0027)

* *chat <jid> <message>* - Sending pgp encrypted message to jid

*-m omemo*:: 
OMEMO Mode (XEP-0384)

* *list* - List the device IDs and fingerprints
* *delete-device-list* - Deletes the OMEMO device list

*-m openpgp*:: 
openpgp mode (XEP-0373)

* *signcrypt <jid> <message>* - Sending pgp signed and encrypted message to jid

*-m monitor*:: 
Monitot mode

* *stanza* - Stanza Monitor
* *monitor* - microblog Monitor microblog (XEP-0277)

*-m bookmark*:: Bookmark mode (XEP-0048)

* *list* - List bookmarks

*-m mam*:: Message Archive Management (XEP-0313)

* *list <jid>* - List messages from <jid>

*-m discovery*:: Service Discovery (XEP-0030)

* *info <jid>* - info request for <jid>
* *item <jid>* - item request for <jid>


ENVIRONMENT VARIABLES
---------------------

* HOME


EXAMPLES
--------
  xmppc --jid user@domain.tld --pwd "secret" --mode roster list
  xmppc --jid user@domain.tld --mode pgp chat friend@domain.tld "Hello"
  xmppc -a account1 --mode discovery item conference@domain.tld
  xmppc --mode bookmark list


FILES
-----
Configuration file to setup accounts.

*~/.config/xmppc.conf*::
Example:


  [default]
  jid=account1@domain.tld
  pwd=password1
  
  [account2]
  jid=account2@domain.tld
  
  [account3]
  jid=account3@domain.tld
  pwd=password3

EXIT STATUS
-----------
*0*::
    Success

*1*::
    Failure 


BUGS
----
See <https://codeberg.org/Anoxinon_e.V./xmppc/issues>


AUTHOR
------
* DebXWoody

RESOURCES
---------
Source: <https://codeberg.org/Anoxinon_e.V./xmppc>

Documentation: <https://codeberg.org/Anoxinon_e.V./xmppc/wiki>


COPYING
-------
Copyright \(C) 2020 Anoxinon e.V. Free use of this software is
granted under the terms of the GNU General Public License (GPL).
